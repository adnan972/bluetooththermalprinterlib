package com.thinknsync.bluetoothposprinter;

public class BlueToothPosDevice implements  BluetoothDeviceModel {

    private String modelNumber;
    private String defaultPassword;

    public BlueToothPosDevice(String modelNumber, String defaultPassword) {
        this.modelNumber = modelNumber;
        this.defaultPassword = defaultPassword;
    }

    @Override
    public String getModelNumber() {
        return modelNumber;
    }

    @Override
    public void setModelNumber(String modelNumber) {
        this.modelNumber=modelNumber;
    }

    @Override
    public String getDefaultPassword() {
        return defaultPassword;
    }

    @Override
    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword=defaultPassword;
    }
}
