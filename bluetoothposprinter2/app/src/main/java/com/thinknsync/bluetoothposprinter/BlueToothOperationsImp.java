package com.thinknsync.bluetoothposprinter;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.UUID;

public class BlueToothOperationsImp implements BlueToothOperations {
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mmDevice,mConnectedDevice;
    private BluetoothSocket mmSocket;
    // needed for communication to bluetooth device / network
    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private Thread workerThread;
    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;
    private Context context;
    BlueToothPosDevice blueToothPosDevice;

    public BlueToothOperationsImp(Context context, BlueToothPosDevice blueToothPosDevice) {
        this.context = context;
        this.blueToothPosDevice = blueToothPosDevice;
    }

    @Override
    public boolean checkBluetoothCompatibility() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter != null;
    }

    @Override
    public boolean isBluetoothEnabled() {
        return mBluetoothAdapter.isEnabled();
    }


    @Override
    public BluetoothDeviceModel getBluetoothDeviceModel() {
        return blueToothPosDevice;
    }

    @Override
    public void setBluetoothDeviceModel(BlueToothPosDevice btDeviceModel) {
        blueToothPosDevice.setModelNumber(btDeviceModel.getModelNumber());
        blueToothPosDevice.setDefaultPassword(btDeviceModel.getDefaultPassword());

    }

    @Override
    public void turnBluetoothOn() {
        if(isBluetoothEnabled()){
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            ((Activity) context).startActivityForResult(enableBluetooth,0);
        }

    }

    @Override
    public Set<BluetoothDevice> scanForBluetoothDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        Log.e("bonded_deivce",pairedDevices.toString());
        return pairedDevices;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean checkForPairedDevice() throws UnsupportedEncodingException {

        if(scanForBluetoothDevices().size() > 0) {

            Log.e("sizetest",scanForBluetoothDevices().size()+"");
            for(BluetoothDevice device : scanForBluetoothDevices()) {
                Log.e("paired device",device.getName());
                // RPP300 is the name of the bluetooth printer device
                // we got this name from the list of paired devices
                Log.e("enter",getBluetoothDeviceModel().getModelNumber()+"");

                if (device.getName().equals(getBluetoothDeviceModel().getModelNumber())) {
                    Log.e("pin",getBluetoothDeviceModel().getDefaultPassword());
                    String pin="0000";
                    device.setPin(pin.getBytes("UTF-8"));
                    mmDevice = device;
                    return  true;
                }
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void pairDevice() {
        try {
            if(checkForPairedDevice()){
                mConnectedDevice=mmDevice;
                Toast.makeText(context,"BlueTooth device Found !",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(context,"Pair With The Device !!",Toast.LENGTH_SHORT).show();
                Intent intentBluetooth = new Intent();
                intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                intentBluetooth.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentBluetooth);
            }
        } catch (UnsupportedEncodingException e) {
            Intent intentBluetooth = new Intent();
            intentBluetooth.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
            Toast.makeText(context,"Pair With The Device !!",Toast.LENGTH_SHORT).show();
            intentBluetooth.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentBluetooth);
            e.printStackTrace();
        }
    }

    @Override
    public void conenctPrinter() {
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createInsecureRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            beginListenForData();
            Toast.makeText(context,"Bluetooth Opened",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printData(String printMsg) {
        try {
            // the text typed by the user
            String msg = printMsg;
            msg += "\n";
            mmOutputStream.write(msg.getBytes());
            // tell the user data were sent
//            view.myLabel.setText("Data sent.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean writeWithFormat(byte[] buffer, byte[] pFormat, byte[] pAlignment) {
        try {
            // Notify printer it should be printed with given alignment:
            mmOutputStream.write(pAlignment);
            // Notify printer it should be printed in the given format:
            mmOutputStream.write(pFormat);
            // Write the actual data:
            mmOutputStream.write(buffer, 0, buffer.length);

            // Share the sent message back to the UI Activity
            //   App.getInstance().getHandler().obtainMessage(MESSAGE_WRITE, buffer.length, -1, buffer).sendToTarget();
            return true;
        } catch (IOException e) {
            // Log.e(TAG, "Exception during write", e);
            return false;
        }
    }


    public void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
//                                                view.showShortMessage(data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
