package com.thinknsync.bluetoothposprinter;

public interface BluetoothDeviceModel {
    String getModelNumber();
    void  setModelNumber(String modelNumber);
    String getDefaultPassword();
    void  setDefaultPassword(String defaultPassword);
}
